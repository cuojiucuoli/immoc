package com.define.test;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.define.bean.User;
import com.define.repository.UserDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyTest {

	@Autowired
	UserDao dao;
	
	@Test
	public void getM() {
		User findUserByUsername = dao.findUserByUsername("admin");
		System.out.println(findUserByUsername);
	}
}
