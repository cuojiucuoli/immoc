package com.define.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.define.bean.User;

@Repository
public interface  UserDao extends JpaRepository<User, Integer>{

	public User findUserByUsername(String username);
}
