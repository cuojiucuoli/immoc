package com.define.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.define.bean.User;
import com.define.repository.UserDao;

@Component
public class UserServiceImp implements UserDetailsService{

	@Autowired
	UserDao dao;
	
	@Override
	public UserDetails loadUserByUsername(String sd) throws UsernameNotFoundException {
		User user = dao.findUserByUsername(sd);
		org.springframework.security.core.userdetails.User user2 = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword() ,AuthorityUtils.commaSeparatedStringToAuthorityList(user.getAuthority()));
		
		return user2;
	}

}
