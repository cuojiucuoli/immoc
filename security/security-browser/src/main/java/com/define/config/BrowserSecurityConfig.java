package com.define.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
@Configuration
public class BrowserSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	SuccessHandler handler;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		super.configure(auth);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin()
			.loginPage("/immoc.html").permitAll()
			.loginProcessingUrl("/auth/login")
			.successHandler(handler)
			.and()		
			.authorizeRequests()			
			.anyRequest()
			.authenticated()
			.and()
			.csrf().disable()
			;
			
		super.configure(http);
	}

	
}
